<?php

return [
    'preheader'       => 'Спешим уведомить, что кто-то ответил на вашу запись на форуме.',
    'greeting'        => 'Добро пожаловать,',
    'body'            => 'Спешим уведомить, что кто-то ответил на вашу запись на форуме',
    'view_discussion' => 'Просмотреть '.mb_strtolower(trans('chatter::intro.titles.discussion')).'.',
    'farewell'        => 'Удачи!',
    'unsuscribe'      => [
        'message' => 'Если Вы уверены, что не хотите больше получать уведомления об ответах на форуме, снимите галочку ниже... :)',
        'action'  => 'Вам не нравятся эти еmail-ы?',
        'link'    => 'Отпишитесь от рассылки на '.mb_strtolower(trans('chatter::intro.titles.discussion')).'.',
    ],
];
