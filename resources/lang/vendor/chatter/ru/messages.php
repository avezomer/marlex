<?php

return [
    'words' => [
        'cancel'  => 'Отмена',
        'delete'  => 'Удалить',
        'edit'    => 'Редактировать',
        'yes'     => 'Да',
        'no'      => 'Нет',
        'minutes' => 'одна минута| :count minute',
    ],

    'discussion' => [
        'new'          => 'Новая '.trans('chatter::intro.titles.discussion'),
        'all'          => 'Все '.trans('chatter::intro.titles.discussion'),
        'create'       => 'Создать '.trans('chatter::intro.titles.discussion'),
        'posted_by'    => 'Опубликовано',
        'head_details' => 'Опубликовано в категории',

    ],
    'response' => [
        'confirm'     => 'Вы уверены, что хотите удалить это сообщение',
        'yes_confirm' => 'Да, удалить!',
        'no_confirm'  => 'Нет, спасибо!',
        'submit'      => 'Отправить ответ',
        'update'      => 'Обновить',
    ],

    'editor' => [
        'title'               => 'Название '.trans('chatter::intro.titles.discussion'),
        'select'              => 'Выберите категорию',
        'tinymce_placeholder' => 'Напишите '.trans('chatter::intro.titles.discussion').' здесь...',
        'select_color_text'   => 'Выберите цвет для '.trans('chatter::intro.titles.discussion').' (по желанию)',
    ],

    'email' => [
        'notify' => 'Уведомить о новых ответах',
    ],

    'auth' => 'Пожалуйста, <a href="/:home/login">войдите в систему</a>
                или <a href="/:home/register">зарегистрируйтесь</a>
                для того, чтобы оставить ответ на форуме.',

];
