<?php

return [
    'success' => [
        'title'  => 'Ура!',
        'reason' => [
            'submitted_to_post'       => 'Ответ был успешно отправлен в '.mb_strtolower(trans('chatter::intro.titles.discussion')).'.',
            'updated_post'            => 'Обновление '.mb_strtolower(trans('chatter::intro.titles.discussion')).' успешно завершено.',
            'destroy_post'            => 'Успешно удалён ответ и '.mb_strtolower(trans('chatter::intro.titles.discussion')).'.',
            'destroy_from_discussion' => 'Успешно удалён ответ из '.mb_strtolower(trans('chatter::intro.titles.discussion')).'.',
            'created_discussion'      => 'Была успешно создана '.mb_strtolower(trans('chatter::intro.titles.discussion')).'.',
        ],
    ],
    'info' => [
        'title' => 'Внимание!',
    ],
    'warning' => [
        'title' => 'Ой-ой!',
    ],
    'danger'  => [
        'title'  => 'Ошибка!',
        'reason' => [
            'errors'            => 'Просьба исправить следующие ошибки:',
            'prevent_spam'      => 'Чтобы не допустить спама, пожалуйста, соблюдайте :minutes минут между ответами.',
            'trouble'           => 'Извините, при попытки отправить ваш ответ произошла ошибка.',
            'update_post'       => 'Ай-ай-ай... Нам не удалось обновить ответ. Вы уверены что не делаете ничего странного?!',
            'destroy_post'      => 'Ай-ай-ай... Нам не удалось удалить ответ. Вы уверены что не делаете ничего странного?!',
            'create_discussion' => 'Опа :( Кажется есть проблема в создании '.mb_strtolower(trans('chatter::intro.titles.discussion')).'.',
            'title_required'    => 'Просьба написать название',
            'title_min'         => 'Название должно содержать минимум :min символов.',
            'title_max'         => 'Название должно содержать максимум :max символов.',
            'content_required'  => 'Просьба заполнить содержание',
            'content_min'       => 'Содержание должно содержать минимум :min символов',
            'category_required' => 'Просьба выбрать категорию',



        ],
    ],
];
