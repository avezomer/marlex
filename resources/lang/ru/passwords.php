<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен содержать минимум 6 символов.',
    'reset' => 'Ваш пароль был успешно сброшен!',
    'sent' => 'Мы отправили Вам на почту ссылку по сбросу пароля!',
    'token' => 'Данный токен пароля недоступен.',
    'user' => "Нам не удалось найти пользователя с данной электронной почтой.",

];
