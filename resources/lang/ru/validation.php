<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute должен быть подтверждён.',
    'active_url'           => ':attribute невалидный URL.',
    'after'                => ':attribute должна быть дата после :date.',
    'after_or_equal'       => ':attribute далжна быть равной или больше даты :date.',
    'alpha'                => ':attribute может содержать только буквы.',
    'alpha_dash'           => ':attribute может содержать только буквы, цифры и тире.',
    'alpha_num'            => ':attribute может содержать только буквы и цифры.',
    'array'                => ':attribute должен быть массив.',
    'before'               => ':attribute должна быть дата до :date.',
    'before_or_equal'      => ':attribute далжна быть равной или меньше даты :date.',
    'between'              => [
        'numeric' => ':attribute должна быть между :min и :max.',
        'file'    => ':attribute должна быть между :min и :max Кб.',
        'string'  => ':attribute должна быть между :min и :max символов.',
        'array'   => ':attribute должна иметь между :min и :max предметов.',
    ],
    'boolean'              => ':attribute поле может быть истино или ложно.',
    'confirmed'            => ':attribute подтверждение не соответствует.',
    'date'                 => ':attribute невалидная дата.',
    'date_format'          => ':attribute несоответствующий формат :format.',
    'different'            => ':attribute и :other должен быть разным.',
    'digits'               => ':attribute должен иметь :digits цифр.',
    'digits_between'       => ':attribute должен иметь между :min и :max цифр.',
    'dimensions'           => ':attribute имеет недопустимое расширение картинки.',
    'distinct'             => ':attribute поле имеет повторяющееся значение.',
    'email'                => ':attribute должен быть валидный почтовый адрес.',
    'exists'               => 'выбранный :attribute невалидный.',
    'file'                 => ':attribute должен быть файлом.',
    'filled'               => ':attribute после должно содержать значение.',
    'image'                => ':attribute должно быть картинкой.',
    'in'                   => 'выбранный :attribute невалидный.',
    'in_array'             => ':attribute после не существует в :other.',
    'integer'              => ':attribute должен быть integer.',
    'ip'                   => ':attribute должен быть валидный IP адрес.',
    'ipv4'                 => ':attribute должен быть валидный IPv4 адрес.',
    'ipv6'                 => ':attribute должен быть валидный IPv6 адрес.',
    'json'                 => ':attribute должна быть валидной JSON строка.',
    'max'                  => [
        'numeric' => ':attribute не можен быть больше чем :max.',
        'file'    => ':attribute не можен быть больше чем :max Кб.',
        'string'  => ':attribute не можен быть больше чем :max символов.',
        'array'   => ':attribute не может содержать больше чем :max предметов.',
    ],
    'mimes'                => ':attribute должен быть файл типа: :values.',
    'mimetypes'            => ':attribute должен быть файл типа: :values.',
    'min'                  => [
        'numeric' => ':attribute должно быть как минимум :min.',
        'file'    => ':attribute должно быть как минимум :min Кб.',
        'string'  => ':attribute должно быть как минимум :min символов.',
        'array'   => ':attribute должно быть меньше чем :min предметов.',
    ],
    'not_in'               => 'выбранный :attribute невалидный.',
    'numeric'              => ':attribute должен быть числом.',
    'present'              => ':attribute поле должно присутствовать.',
    'regex'                => ':attribute невалидный формат.',
    'required'             => ':attribute поле обязательно.',
    'required_if'          => ':attribute поле обязательно когда :other -> :value.',
    'required_unless'      => ':attribute поле обязательно unless :other в :values.',
    'required_with'        => ':attribute поле обязательно когда :values присутствует.',
    'required_with_all'    => ':attribute поле обязательно когда :values присутствует.',
    'required_without'     => ':attribute поле обязательно когда :values не присутствует.',
    'required_without_all' => ':attribute поле обязательно когда ни одного :values не присутствует.',
    'same'                 => ':attribute и :other должны соответствует.',
    'size'                 => [
        'numeric' => ':attribute должен иметь :size.',
        'file'    => ':attribute должен иметь :size Кб.',
        'string'  => ':attribute должен иметь :size символов.',
        'array'   => ':attribute должен содержать :size предметов.',
    ],
    'string'               => ':attribute должен быть строкой.',
    'timezone'             => ':attribute должен быть верный часовой пояс.',
    'unique'               => ':attribute уже существует.',
    'uploaded'             => ':attribute ошибка при загрузки.',
    'url'                  => ':attribute невалидный формат.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
