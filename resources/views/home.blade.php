@extends('layouts.app')
@section('title', 'MarLex :: Главная')

@section('content_header')
    <?$colors = ['aqua', 'red', 'green', 'purple', 'yellow']?>
    <?$icons = ['envelope', 'comments', 'camera', 'newspaper-o']?>
    <?$types = ['topic' => 'тема', 'chapter' => 'глава', 'chatter' => 'запись в теме'];?>
<ul class="timeline">
    @foreach($data as $date => $items)
    <!-- timeline time label -->
    <li class="time-label">
        <span class="bg-{!! $colors[array_rand($colors)] !!}">
            {!! $date !!}
        </span>
    </li>
    <!-- /.timeline-label -->

    @foreach($items as $post)
    <!-- timeline item -->
    <li>
        <!-- timeline icon -->
        <i class="fa fa-{!! $icons[array_rand($icons)] !!} bg-{!! $colors[array_rand($colors)] !!}"></i>
        <div class="timeline-item">
            <span class="time"><i class="fa fa-clock-o"></i> {!! $post['created_time'] !!}</span>

            <h3 class="timeline-header"><a href="#">{!! $post['author_name'] !!}</a></h3>

            <div class="timeline-body">
                Была добавлена новая {!! $types[$post['type']] !!} : {!! $post['item_name'] !!}
            </div>

            <div class="timeline-footer">
                <a class="btn btn-primary btn-xs">просмотреть ...</a>
            </div>
        </div>
    </li>
    @endforeach
    @endforeach
    <!-- END timeline item -->

    <li class="time-label">
    <span class="bg-{!! $colors[array_rand($colors)] !!}">
        конец
    </span>
    </li>


</ul>

@endsection

@section('content')
    <div class="text-center">конец...</div>
@endsection