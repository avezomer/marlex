@extends('layouts.app')
@section('title', 'MarLex :: Темы')

@section('content_header')
    <h1>Темы</h1>
@endsection

@section('content')
    @include('global.message')
    <a href="{{ URL::to('topics/create') }}" class="btn btn-success">Добавить тему</a>
    {!!
        $topics->columns([
            'id' => 'ID',
            'name' => 'Название',
            'alias' => 'Алиас',
            'chapter_id' => 'Глава',
            'author_id' => 'Автор',
            'actions' => 'Действия'
        ])
        ->means('name', 'topic')
        ->modify('name', function ($i, $topic){
            return "<a href='".URL::to('topics')."/{$topic->id}'>{$topic->name}</a>";
        })
        ->means('chapter_id', 'chapter')
        ->modify('chapter_id', function ($i, $topic){
            return $topic->chapters->name;
        })
        ->means('author_id', 'author')
        ->modify('author_id', function ($i, $topic){
            return $topic->authors->name;
        })
        ->means('actions', 'action')
        ->modify('actions', function ($i, $topic){
            return
                Form::open(['url' => "topics/{$topic->id}", 'class' => 'pull-right']) .
                Form::hidden('_method', 'DELETE') .
                Form::submit('x', ['class' => 'btn btn-danger']).
                Form::close() .
                HTML::link(URL::to("topics/{$topic->id}/edit"), '' ,['class' => 'fa fa-edit fa-3x text-orange pull-right']);
        })
        ->attributes([
            'id' => 'results',
            'class' => 'table table-striped table-bordered text-center',
        ])
        ->render()
    !!}
@endsection
@section('js')
@endsection

