@extends('layouts.app')
@section('title', 'MarLex :: Привязка заданий')

@section('content_header')
    <h1>Задания</h1>
@endsection

@section('content')
    <div class="box box-info col-md-3">
        <div class="box-header with-border">
            <h3 class="box-title">Привязка</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['route' => 'exercises.store']) !!}
         <div class="box-body" style="min-height: 100%;">
            <input type="hidden" name="action" value="link">
            <div class="form-group">
                {{ Form::label('modules', 'Модуль', ['class' => 'control-label col-sm-4 text-right']) }}
                <div class="col-sm-8">
                    <div class="col-sm-8">
                        <select class="form-control" id="modules" name="modules[]" multiple>
                            <optgroup label="Темы">
                            @foreach($topics as $topic)
                                <option value='{{"topic::$topic->id"}}'>{{$topic->name}}</option>
                            @endforeach
                            </optgroup>
                            <optgroup label="Тесты">
                            @foreach($tests as $test)
                                <option value='{{"test::$test->id"}}'>{{$test->name}}</option>
                            @endforeach
                            </optgroup>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('type_of_exercise_id', 'Задания', ['class' => 'control-label col-sm-4 text-right']) }}
                <div class="col-sm-8">
                    <div class="col-sm-8">
                        <select class="form-control" id="exercises" name="exercises[]" multiple>
                            @foreach($exercises as $typeOfExercise)
                                <optgroup label="{{$typeOfExercise['name']}}">
                                    @foreach($typeOfExercise['exercises'] as $exercise)
                                    <option value="{{$exercise['id']}}">{{$exercise['name']}}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group" style="padding-bottom: 200px;">
                <div class="text-center">
                {{ Form::submit('Добавить', ['class' => 'btn btn-success']) }}
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>

@endsection
@section('js')
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <script type="text/javascript">

$(document).ready(function(){
    $('select[multiple]').multiselect({
        nonSelectedText: 'выберите значение',
        buttonWidth:'100%',
        enableClickableOptGroups: true
    });
});
</script>
@endsection
