@extends('layouts.app')
@section('title', 'MarLex :: Задания')

@section('content_header')
    <h1>Задания</h1>
@endsection
{{--
<style>
    ul#sortable { list-style-type: none; margin: 0; padding: 0; margin-bottom: 10px; }
    #sortable li { height: 100px;width: 30px;float: left; text-align: center; text-valing:center;}
</style>
--}}

@section('content')
<div class="text-center">
    <div class="info-box bg-blue" style="width: 50%; display: inline-block;">
        <span class="info-box-icon">
        <i class="ion ion-thumbsup"></i></span>
        <div class="info-box-content">
            <span class="info-box-text">{{ $exercise->typeOfExercises->name }}</span>
            <div class="info-box-number">{!! $exercise->text !!}</div>
        </div>
        <hr>
        <div class="text-center">
            <buttons class="btn btn-warning">Пропустить</buttons>
            <buttons class="btn btn-success">Ответить</buttons>
        </div>

        <!-- /.info-box-content -->
    </div>
</div>

    <div class="math-tex">


        {{--
            <ul id="sortable">
            @foreach($temp as $item)
                    <li class="ui-state-default">
                        $${{$item}}$$
                    </li>
            @endforeach
            </ul>
        --}}
    </div>

@endsection
@section('js')
    @include('global.mathjax')
{{--
    <script>
        $( function() {
            $( "#sortable" ).sortable({
                revert: true
            });
            $( "#draggable" ).draggable({
                connectToSortable: "#sortable",
                helper: "clone",
                revert: "invalid"
            });
            $( "ul, li" ).disableSelection();
        } );
    </script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
--}}
@endsection

