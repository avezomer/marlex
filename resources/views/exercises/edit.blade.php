@extends('layouts.app')
@section('title', 'MarLex :: Редактрование заданий')

@section('content_header')
    <h1>Редактрование заданий</h1>
@endsection

@section('content')
    <div class="box box-info col-md-3">
        <div class="box-header with-border">
            <h3 class="box-title">Редактрование</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($topic, ['route' => ['topics.update', $topic->id], 'method' => 'PUT']) !!}
        <div class="box-body">
            <div class="form-group">
                    {{ Form::label('name', 'Название', ['class' => 'control-label col-sm-4 text-right']) }}
                <div class="col-sm-8">
                    <div class="col-sm-8">
                        {{ Form::text('name', $topic->name, ['class' => 'form-control col-sm-4', 'placeholder' => 'навзвание' ]) }}
                    </div>
                </div>
            </div>
            <div class="form-group">
                    {{ Form::label('alias', 'Алиас', ['class' => 'control-label col-sm-4 text-right']) }}
                <div class="col-sm-8">
                    <div class="col-sm-8">
                        {{ Form::text('alias', $topic->alias, ['class' => 'form-control col-sm-4', 'placeholder' => 'ссылка' ]) }}
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('text', 'Текст', ['class' => 'control-label col-sm-4 text-right']) }}
                <div class="col-sm-8">
                    <div class="col-sm-8">
                        {{ Form::textarea('text', $topic->text, ['id' => 'ckeditor', 'class' => 'form-control col-sm-4']) }}
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('chapter_id', 'Глава', ['class' => 'control-label col-sm-4 text-right']) }}
                <div class="col-sm-8">
                    <div class="col-sm-8">
                        {{ Form::select('chapter_id', $chapters, $topic->chapter_id, ['class' => 'form-control col-sm-4']) }}
                    </div>
                </div>
            </div>
            <hr>
            <div class="text-center">
                {{ Form::submit('Сохранить', ['class' => 'btn btn-success']) }}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
@section('js')
    @include('global.ckeditor')
    @include('global.mathjax')
@endsection