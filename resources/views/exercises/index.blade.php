@extends('layouts.app')
@section('title', 'MarLex :: Задания')

@section('content_header')
    <h1>Задания</h1>
@endsection

@section('content')
    @include('global.message')
    <a href="{{ URL::to('exercises/create') }}" class="btn btn-success">Добавить задание</a>
    <a href="{{ URL::to('exercises/link') }}" class="btn btn-info">Привязать задание</a>
    <a href="{{ URL::to('exercises/delete_results') }}" class="btn btn-danger">Очистить результаты</a>
    {!!
        $exercises->columns([
            'id' => 'ID',
            'name' => 'Название',
            'points' => 'Кол-во баллов',
            'type_of_exercise_id' => 'тип',
            'actions' => 'Действия'
        ])
        ->means('name', 'link_to_name')
        ->modify('name', function ($i, $exercise){
            return "<a href='". URL::to('exercises/'.$exercise->id) ."'>{$exercise->name}</a>";
        })
        ->means('type_of_exercise_id', 'type')
        ->modify('type_of_exercise_id', function ($i, $exercise){
            return $exercise->typeOfExercises->name;
        })
        ->means('actions', 'action')
        ->modify('actions', function ($i, $exercise){
            return
                Form::open(['url' => "exercises/{$exercise->id}", 'class' => 'pull-right']) .
                Form::hidden('_method', 'DELETE') .
                Form::submit('x', ['class' => 'btn btn-danger']).
                Form::close() .
                HTML::link(URL::to("exercises/{$exercise->id}/edit"), '' ,['class' => 'fa fa-edit fa-3x text-orange pull-right']);
        })
        ->attributes([
            'id' => 'results',
            'class' => 'table table-striped table-bordered text-center',
        ])
        ->render()
    !!}
@endsection
@section('js')
@endsection

