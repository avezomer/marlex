<template id="variant">
    <div class="form-group adds">
        {{ Form::label('variant[]', 'Вариант ответа', ['class' => 'control-label col-sm-4 text-right']) }}
        <div class="col-sm-8">
            <div class="col-sm-8">
                {{ Form::textarea('variant[]', null, ['id' => '{ID}', 'class' => 'form-control col-sm-4 ckeditor']) }}
            </div>
            <i class="fa fa-2x fa-plus text-green addNewRow" style="cursor: pointer;"></i>
            <i class="fa fa-2x fa-minus text-red removeSelectedRow hidden" style="cursor: pointer;"></i>
        </div>
    </div>
</template>
<template id="ckeditor">
    <div class="form-group adds">
        {{ Form::label('{NAME}', '{LABEL}', ['class' => 'control-label col-sm-4 text-right']) }}
        <div class="col-sm-8">
            <div class="col-sm-8">
                {{ Form::textarea('{NAME}', null, ['id' => '{ID}', 'class' => 'form-control col-sm-4 ckeditor']) }}
            </div>
            <i class="fa fa-2x fa-minus text-red removeSelectedRow" style="cursor: pointer;"></i>
        </div>
    </div>
</template>
@extends('layouts.app')
@section('title', 'MarLex :: Добавление задания')

@section('content_header')
    <h1>Задания</h1>
@endsection

@section('content')
    <div class="box box-info col-md-3">
        <div class="box-header with-border">
            <h3 class="box-title">Добавление</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['route' => 'exercises.store']) !!}
        <div class="box-body">
            <div class="form-group">
                    {{ Form::label('name', 'Название', ['class' => 'control-label col-sm-4 text-right']) }}
                <div class="col-sm-8">
                    <div class="col-sm-8">
                        {{ Form::text('name', null, ['class' => 'form-control col-sm-4', 'placeholder' => 'навзвание']) }}
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('type_of_exercise_id', 'Тип', ['class' => 'control-label col-sm-4 text-right']) }}
                <div class="col-sm-8">
                    <div class="col-sm-8">
                        <select class="form-control col-sm-4" id="type_of_exercise_id" name="type_of_exercise_id">
                            @foreach($typeOfExercises as $id => $type)
                                <option value="{{$id}}" data-id="{{$type['alias']}}">{{$type['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                    {{ Form::label('text', 'Текст', ['class' => 'control-label col-sm-4 text-right']) }}
                <div class="col-sm-8">
                    <div class="col-sm-8">
                        {{ Form::textarea('text', null, ['id' => 'ckeditor0', 'class' => 'form-control col-sm-4 ckeditor']) }}
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('answer[]', 'Ответ', ['class' => 'control-label col-sm-4 text-right']) }}
                <div class="col-sm-8">
                    <div class="col-sm-8">
                        {{ Form::textarea('answer[]', null, ['id' => 'ckeditor1', 'class' => 'form-control col-sm-4 ckeditor']) }}
                    </div>
                    <i class="fa fa-2x fa-plus text-green addNewRow" style="cursor: pointer;"></i>
                    <i class="fa fa-2x fa-minus text-red removeSelectedRow hidden" style="cursor: pointer;"></i>
                </div>
            </div>
            <div class="form-group">
                    {{ Form::label('points', 'Баллы', ['class' => 'control-label col-sm-4 text-right']) }}
                <div class="col-sm-8">
                    <div class="col-sm-8">
                        {{ Form::selectRange('points', 1, 10, null, ['class' => 'form-control col-sm-4']) }}
                    </div>
                </div>
            </div>
            <hr>
            <div class="text-center">
                {{ Form::submit('Добавить', ['class' => 'btn btn-success']) }}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
@section('js')
    @include('global.ckeditor')
    @include('global.mathjax')
    <script>
        var init = function () {
            _getTypeOfExercise();
            _addNewRow();
            _removeSelectedRow();
        }();

        function _getTypeOfExercise() {
            $(document).on('change', '[name="type_of_exercise_id"]', function () {
                var $this = $(this);
                var key = $this.find('option[value="'+$this.val()+'"]').data('id');
                _removeAdds();
                if (key === 'choose_from_several') {
                    var $variant = $('#variant').html();
                    var newID = 'ckeditor' + $(document).find('.ckeditor').length;
                    $variant = $variant.replace('{ID}', newID);
                    $this.closest('.form-group').after($variant);
                    CKEDITOR.replace(newID);
                }

            })

        }

        function _removeAdds() {
            $(document).find('.box-body .adds').remove();
        }

        function _addNewRow() {
            $(document).on('click', '.addNewRow', function () {
                var name = $(this).closest('.form-group').find('textarea').attr('name');
                var label = $(this).closest('.form-group').find('label').text();
                var newID = 'ckeditor' + $(document).find('.ckeditor').length;
                var $ckeditor = $('#ckeditor').html();
                $ckeditor = $ckeditor.replace(/{NAME}/g, name).replace(/{LABEL}/g, label).replace(/{ID}/g, newID);
                $(this).closest('.form-group').after($ckeditor);
                CKEDITOR.replace(newID);
            });
        }

        function _removeSelectedRow() {
            $(document).on('click', '.removeSelectedRow', function () {
                $row = $(this).closest('.form-group').remove();
            });
        }

    </script>
@endsection