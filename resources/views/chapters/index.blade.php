@extends('layouts.app')
@section('title', 'MarLex :: Главы')

@section('content_header')
    <h1>Список глав</h1>
@endsection

@section('content')
    @include('global.message')
    <a href="{{ URL::to('chapters/create') }}" class="btn btn-success">Добавить главу</a>
    {!!
        $chapters->columns([
            'id' => 'ID',
            'name' => 'Название',
            'actions' => 'Действия'
        ])
        ->means('actions', 'action')
        ->modify('actions', function ($i, $chapter){
            return
                Form::open(['url' => "chapters/{$chapter->id}", 'class' => 'pull-right']) .
                Form::hidden('_method', 'DELETE') .
                Form::submit('x', ['class' => 'btn btn-danger']).
                Form::close() .
                HTML::link(URL::to("chapters/{$chapter->id}/edit"), '' ,['class' => 'fa fa-edit fa-3x text-orange pull-right']);
        })
        ->attributes([
            'id' => 'results',
            'class' => 'table table-striped table-bordered text-center',
        ])
        ->render()
    !!}
@endsection
@section('js')
@endsection

