<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML"></script>
<script>
    MathJax.Hub.Config({
        TeX: {
            equationNumbers: { autoNumber: "AMS" },
            Macros: {
                RR: "{\\bf R}",
                bold: ["{\\bf #1}",1]
            },
            tex2jax: {
                inlineMath: [ ['$','$'], ['\\(','\\)'] ]
            }
        }
    });
</script>
