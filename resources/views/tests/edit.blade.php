@extends('layouts.app')
@section('title', 'MarLex :: Редактрование теста')

@section('content_header')
    <h1>Тесты</h1>
@endsection

@section('content')
    <div class="box box-info col-md-3">
        <div class="box-header with-border">
            <h3 class="box-title">Редактрование</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($test, ['route' => ['tests.update', $test->id], 'method' => 'PUT']) !!}
        <div class="box-body">
            <div class="form-group">
                    {{ Form::label('name', 'Название', ['class' => 'control-label col-sm-4 text-right']) }}
                <div class="col-sm-8">
                    <div class="col-sm-8">
                        {{ Form::text('name', $test->name, ['class' => 'form-control col-sm-4', 'placeholder' => 'навзвание' ]) }}
                    </div>
                </div>
            </div>
            <div class="form-group">
                    {{ Form::label('short_name', 'Сокращённое название', ['class' => 'control-label col-sm-4 text-right']) }}
                <div class="col-sm-8">
                    <div class="col-sm-8">
                        {{ Form::text('short_name', $test->short_name, ['class' => 'form-control col-sm-4', 'placeholder' => 'навзвание' ]) }}
                    </div>
                </div>
            </div>
            <div class="form-group">
                    {{ Form::label('alias', 'Алиас', ['class' => 'control-label col-sm-4 text-right']) }}
                <div class="col-sm-8">
                    <div class="col-sm-8">
                        {{ Form::text('alias', $test->alias, ['class' => 'form-control col-sm-4', 'placeholder' => 'ссылка' ]) }}
                    </div>
                </div>
            </div>
            <hr>
            <div class="text-center">
                {{ Form::submit('Сохранить', ['class' => 'btn btn-success']) }}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection