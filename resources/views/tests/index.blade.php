@extends('layouts.app')
@section('title', 'MarLex :: Главы')

@section('content_header')
    <h1>Список тестов</h1>
@endsection

@section('content')
    @include('global.message')
    <a href="{{ URL::to('tests/create') }}" class="btn btn-success">Добавить тест</a>
    {!!
        $tests->columns([
            'id' => 'ID',
            'name' => 'Название',
            'actions' => 'Действия'
        ])
        ->means('name', 'test')
        ->modify('name', function ($i, $topic){
            return "<a href='".URL::to('tests')."/{$topic->id}'>{$topic->name}</a>";
        })
        ->means('actions', 'action')
        ->modify('actions', function ($i, $test){
            return
                Form::open(['url' => "tests/{$test->id}", 'class' => 'pull-right']) .
                Form::hidden('_method', 'DELETE') .
                Form::submit('x', ['class' => 'btn btn-danger']).
                Form::close() .
                HTML::link(URL::to("tests/{$test->id}/edit"), '' ,['class' => 'fa fa-edit fa-3x text-orange pull-right']);
        })
        ->attributes([
            'id' => 'results',
            'class' => 'table table-striped table-bordered text-center',
        ])
        ->render()
    !!}
@endsection
@section('js')
@endsection

