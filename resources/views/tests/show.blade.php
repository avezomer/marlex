@extends('layouts.app')
@section('title', 'MarLex :: Тесты')

@section('content_header')
    <h1></h1>
@endsection

@section('content')
    @include('global.message')
    <div class="text-center">
        <h2>{!! $test->name !!}</h2>
        <div class="exercises">
            @if($testsExercises)
                <div class="text-center">
                    <div class="info-box bg-blue">
                        <span class="info-box-icon">
                        <i class="ion ion-thumbsup"></i></span>
                        <div class="info-box-content">
                            <div class="info-box-number">Для данного теста имеется {{count($testsExercises)}} {!! \App\Core\Utils::declOfNouns(count($testsExercises), ['вопрос', 'вопроса', 'вопросов']); !!}</div>
                        </div>
                        <div class="text-center">
                            <buttons class="btn btn-warning">Пропустить</buttons>
                            <buttons class="btn btn-success answerTheQuestions" data-toggle="modal" data-target="#modal-default">Ответить</buttons>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Задания</h4>
              </div>
              <div class="modal-body">
                <div>
                    <div class="box box-solid">
                        <div class="box-header with-border">
                          <h3 class="box-title">Выполните следующие задания</h3>
                        </div>
                          <!-- /.box-header -->
                        {!! Form::open(['action' => 'TopicsController@evaluate']) !!}
                        <input name="module_id" type="hidden" value='{!! $test->id !!}'>
                        <div class="box-body">
                          <div class="box-group" id="accordion">
                            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                              @foreach($testsExercises as $key => $exercise)
                                <div class="panel box box-primary">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#{{"{$exercise->type_alias}{$exercise->id}"}}" aria-expanded="true" class="">
                                        {{$exercise->type_of_exercise}}
                                        </a>
                                        </h4>
                                    </div>
                                    <div id="{{"{$exercise->type_alias}{$exercise->id}"}}" class="panel-collapse collapse" aria-expanded="true" style="">
                                        <div class="box-body">
                                            <div class="text-center">
                                                <input name="real_answers[{!! $exercise->id !!}][answer]" type="hidden" value='{{$exercise->answer}}'>
                                                <input name="real_answers[{!! $exercise->id !!}][type]" type="hidden" value='{{$exercise->type_alias}}'>
                                                @switch($exercise->type_alias)
                                                    @case('choose_from_several')
                                                        <?$data = json_decode($exercise->text);?>
                                                        {!! $data->text !!}
                                                        @foreach(explode('@', $data->variants) as $item)
                                                            <div class="alert alert-info col-md-6" style="display: inline-block; height: 150px;">
                                                                <input name="answers[{!! $exercise->id !!}][]" type="<?=count(explode('@', $exercise->answer)) > 1 ? 'checkbox':'radio'?>" value='{!! $item !!}' style="zoom: 3;" class="pull-left">
                                                                {!! $item !!}
                                                            </div>
                                                        @endforeach
                                                        @break
                                                    @case('missing_values')
                                                        {!! str_replace('@', '<input name="answers['.$exercise->id.'][]">', $exercise->text) !!}
                                                        @break
                                                    @default
                                                    {!! $exercise->text !!}
                                                    {{ Form::textarea("answers[{$exercise->id}][]", null, ['id' => "ckeditor{$key}", 'class' => 'ckeditor form-control col-sm-4']) }}
                                                @endswitch
                                        </div>
                                    </div>
                                </div>
                              @endforeach
                          </div>
                        </div>

                      <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Отменить</button>
                        {{ Form::submit('Отправить на выполнение', ['class' => 'btn btn-success']) }}
                      </div>
                    {!! Form::close() !!}
                    <!-- /.box-body -->
                    </div>
                </div>
              </div>
            </div>
              <!-- /.modal-content -->
          </div>
        <!-- /.modal-dialog -->
        </div>
@endsection
@section('js')
    @include('global.ckeditor')
    @include('global.mathjax')
@endsection
