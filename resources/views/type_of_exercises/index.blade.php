@extends('layouts.app')
@section('title', 'MarLex :: Типы заданий')

@section('content_header')
    <h1>Типы заданий</h1>
@endsection

@section('content')
    @include('global.message')
    <a href="{{ URL::to('type_of_exercises/create') }}" class="btn btn-success">Добавить задание</a>
    {!!
        $typeOfExercises->columns([
            'id' => 'ID',
            'name' => 'Название',
            'actions' => 'Действия'
        ])
        ->means('actions', 'action')
        ->modify('actions', function ($i, $typeOfExercise){
            return
                Form::open(['url' => "type_of_exercises/{$typeOfExercise->id}", 'class' => 'pull-right']) .
                Form::hidden('_method', 'DELETE') .
                Form::submit('x', ['class' => 'btn btn-danger']).
                Form::close() .
                HTML::link(URL::to("type_of_exercises/{$typeOfExercise->id}/edit"), '' ,['class' => 'fa fa-edit fa-3x text-orange pull-right']);
        })
        ->attributes([
            'id' => 'results',
            'class' => 'table table-striped table-bordered text-center',
        ])
        ->render()
    !!}
@endsection
@section('js')
@endsection

