@extends('layouts.app')
@section('title', 'MarLex :: Создание задания')

@section('content_header')
    <h1>Добавление задания</h1>
@endsection

@section('content')
    <div class="box box-info col-md-3">
        <div class="box-header with-border">
            <h3 class="box-title">Добавление</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['route' => 'type_of_exercises.store']) !!}
        <div class="box-body">
            <div class="form-group">
                    {{ Form::label('name', 'Название', ['class' => 'control-label col-sm-4 text-right']) }}
                <div class="col-sm-8">
                    <div class="col-sm-8">
                        {{ Form::text('name', null, ['class' => 'form-control col-sm-4', 'placeholder' => 'навзвание']) }}
                    </div>
                </div>
            </div>
            <div class="form-group">
                    {{ Form::label('alias', 'Алиас', ['class' => 'control-label col-sm-4 text-right']) }}
                <div class="col-sm-8">
                    <div class="col-sm-8">
                        {{ Form::text('alias', null, ['class' => 'form-control col-sm-4', 'placeholder' => 'ссылка']) }}
                    </div>
                </div>
            </div>
            <hr>
            <div class="text-center">
                {{ Form::submit('Добавить', ['class' => 'btn btn-success']) }}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection