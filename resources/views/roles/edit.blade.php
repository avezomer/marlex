@extends('layouts.app')
@section('title', 'MarLex :: Редактрование роли')

@section('content_header')
    <h1>Редактрование роли</h1>
@endsection

@section('content')
    <div class="box box-info col-md-3">
        <div class="box-header with-border">
            <h3 class="box-title">Редактрование</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($role, ['route' => ['roles.update', $role->id], 'method' => 'PUT']) !!}
        <div class="box-body">
            <div class="form-group">
                    {{ Form::label('name', 'Название', ['class' => 'control-label col-sm-4 text-right']) }}
                <div class="col-sm-8">
                    <div class="col-sm-8">
                        {{ Form::text('name', $role->name, ['class' => 'form-control col-sm-4', 'placeholder' => 'навзвание' ]) }}
                    </div>
                </div>
            </div>
            <div class="form-group">
                    {{ Form::label('guard_name', 'Алиас', ['class' => 'control-label col-sm-4 text-right']) }}
                <div class="col-sm-8">
                    <div class="col-sm-8">
                        {{ Form::text('guard_name', $role->guard_name, ['class' => 'form-control col-sm-4', 'placeholder' => 'навзвание']) }}
                    </div>
                </div>
            </div>
            <hr>
            <div class="text-center">
                {{ Form::submit('Сохранить', ['class' => 'btn btn-success']) }}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection