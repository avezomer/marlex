@extends('layouts.app')
@section('title', 'MarLex :: Главная')

@section('content_header')
    <h1>Список ролей</h1>
@endsection

@section('content')
    @include('global.message')
    <a href="{{ URL::to('roles/create') }}" class="btn btn-success">Добавить роль</a>
    {!!
        $roles->columns([
            'id' => 'ID',
            'name' => 'Название',
            'guard_name' => 'Алиас',
            'actions' => 'Действия'
        ])
        ->means('actions', 'action')
        ->modify('actions', function ($i, $role){
            return
                Form::open(['url' => "roles/{$role->id}", 'class' => 'pull-right']) .
                Form::hidden('_method', 'DELETE') .
                Form::submit('x', ['class' => 'btn btn-danger']).
                Form::close() .
                HTML::link(URL::to("roles/{$role->id}/edit"), '' ,['class' => 'fa fa-edit fa-3x text-orange pull-right']);
        })
        ->attributes([
            'id' => 'results',
            'class' => 'table table-striped table-bordered text-center',
        ])
        ->render()
    !!}
@endsection
@section('js')
@endsection

