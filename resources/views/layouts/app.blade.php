@extends('adminlte::page')
@section('css')
    @yield('css')
@stop
@section('title', 'MarLex')

@section('content')
    @yield('content')
@stop

@section('js')
    @yield('js')
@stop