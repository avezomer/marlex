<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinkedExercisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('linked_exercises', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('item_id');
            $table->enum('item_type', ['topic', 'test']);
            $table->unsignedInteger('exercise_id');
            $table->foreign('exercise_id')->references('id')->on('exercises');
            $table->timestamps();
            $table->unique(['item_id', 'type', 'exercise_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('linked_exercises');
    }
}
