<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExercisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercises', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->text('text');
            $table->string('answer');
            $table->unsignedTinyInteger('points');
            $table->unsignedInteger('type_of_exercise_id');
            $table->timestamps();
            $table->foreign('type_of_exercise_id')->references('id')->on('type_of_exercises');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exercises');
    }
}
