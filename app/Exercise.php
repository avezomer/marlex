<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Exercise extends Model
{
    use \Stevebauman\EloquentTable\TableTrait;
    protected $table = 'exercises';

    public static function add($data)
    {
        $exercise = new self();
        $exercise->name = $data->name;
        $exercise->type_of_exercise_id = $data->type_of_exercise_id;
        $exercise->points = $data->points;
        switch ($data->type_of_exercise_id) {
            case 2:
                $exercise->text = json_encode(['text' => $data->text, 'variants' => implode('@', $data->variant)], JSON_UNESCAPED_UNICODE);
                $exercise->answer = implode('@', $data->answer);
                break;
            case 4: break;
            default:
                $exercise->text = $data->text;
                $exercise->answer = implode('@', $data->answer);
                break;
        }
        return $exercise->save();
    }

    public static function edit($id, $data)
    {
        $exercise = self::find($id);
        $exercise->name = $data->name;
        $exercise->text = $data->text;
        $exercise->answer = implode(',', $data->answer);
        $exercise->points = $data->points;
        $exercise->type_of_exercise_id = $data->type_of_exercise_id;
        return $exercise->save();
    }

    public function typeOfExercises()
    {
        return $this->belongsTo('App\TypeOfExercise', 'type_of_exercise_id');
    }

    public static function getExercisesByTypeOfExercises()
    {
        $data = self::with('typeOfExercises')->get();
        $temp = [];
        foreach ($data as $item) {
            $temp[$item->typeOfExercises->id]['id'] = $item->typeOfExercises->id;
            $temp[$item->typeOfExercises->id]['name'] = $item->typeOfExercises->name;
            $temp[$item->typeOfExercises->id]['alias'] = $item->typeOfExercises->alias;
            $temp[$item->typeOfExercises->id]['exercises'][$item->id]['id'] = $item->id;
            $temp[$item->typeOfExercises->id]['exercises'][$item->id]['name'] = $item->name;
            $temp[$item->typeOfExercises->id]['exercises'][$item->id]['alias'] = $item->alias;
        }

        return $temp;
    }

    public static function link($data)
    {
        $temp = $delete = [];
        foreach ($data->modules as $module) {
            list($moduleType, $moduleID) = explode('::', $module);
            foreach ($data->exercises as $exercise) {
                $temp[] = "({$moduleID}, '{$moduleType}', {$exercise})";
                $delete[] = "item_id = {$moduleID} AND item_type = '{$moduleType}'";
            }
        }

        $deleteQuery = "DELETE FROM linked_exercises WHERE " . implode(' OR ', $delete);
        \DB::delete($deleteQuery);
        $insertQuery = 'INSERT into linked_exercises (item_id, item_type, exercise_id) VALUES ' . implode(',', $temp);

        return \DB::insert($insertQuery);
    }

    public static function evaluate($data, $module = 'topic')
    {
        $userID = Auth::user()->id;
        $temp = [];

        foreach ($data['real_answers'] as $exerciseID => $answer) {
            $realAnswer = strip_tags($answer['answer']);
            if (isset($data['answers'][$exerciseID])) {
                $userAnswer = strip_tags(implode(',', $data['answers'][$exerciseID]));
                similar_text($realAnswer, $userAnswer, $percent);
                $percent = $answer['type'] == 'choose_from_several' ? floor((float)$percent/100) * 100 : round($percent, 2);
            } else $percent = 0;
            $temp[] = "($userID, {$exerciseID}, {$percent}, '{$module}')";
        }
        $values = implode(',', $temp);
        return \DB::insert("INSERT INTO points(user_id, exercise_id, value, module) VALUES {$values}");
    }

    public static function getFinishedExercises($module)
    {
        $userID = Auth::user()->id;
        return array_keys(\DB::table('points')->where([['module', 'topic'], ['user_id', $userID]])->get()->keyBy('exercise_id')->toArray());
    }

    public static function getFinishedExercisesWithPointsByType()
    {
        $userID = Auth::user()->id;
        return \DB::select("
        SELECT
            type_of_exercises.name,
            ROUND(AVG(points.value), 2) AS percent,
            COUNT(*) AS total
        FROM
            exercises
            JOIN type_of_exercises ON exercises.type_of_exercise_id = type_of_exercises.id
            JOIN points ON exercises.id = points.exercise_id
        WHERE points.user_id = {$userID}
        GROUP BY type_of_exercises.id");
    }

    public static function getModuleExercises($moduleID, $moduleName = 'topic')
    {
        $finishedExercises = implode(',', Exercise::getFinishedExercises($moduleName));
        $filter = $finishedExercises ? " AND exercises.id NOT IN($finishedExercises)" : '';
        return \DB::select("
            SELECT
                type_of_exercises.alias AS type_alias,
                type_of_exercises.name AS type_of_exercise,
                exercises.id,
                exercises.name,
                exercises.text,
                exercises.answer,
                exercises.points
            FROM
                exercises
                JOIN type_of_exercises ON exercises.type_of_exercise_id = type_of_exercises.id
                JOIN linked_exercises ON exercises.id = linked_exercises.exercise_id
                JOIN {$moduleName}s ON linked_exercises.item_id = {$moduleName}s.id AND linked_exercises.item_type = '{$moduleName}'
            WHERE
                {$moduleName}s.id = {$moduleID}
                {$filter}");
    }

}
