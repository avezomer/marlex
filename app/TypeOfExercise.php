<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeOfExercise extends Model
{
    use \Stevebauman\EloquentTable\TableTrait;
    protected $table = 'type_of_exercises';

    public static function add($data)
    {
        $typeOfExercises = new TypeOfExercise();
        $typeOfExercises->name = $data->name;
        $typeOfExercises->alias = $data->alias;
        return $typeOfExercises->save();
    }

    public static function edit($id, $data)
    {
        $typeOfExercises = TypeOfExercise::find($id);
        $typeOfExercises->name = $data->name;
        $typeOfExercises->alias = $data->alias;
        return $typeOfExercises->save();
    }

    public function exercises()
    {
        return $this->hasMany('App\Exercise', 'type_of_exercise_id');
    }

    public static function getRecords()
    {
        $typeOfExercises = TypeOfExercise::all();
        $result = [];
        foreach ($typeOfExercises as $typeOfExercise) {
            $result[$typeOfExercise->id]['name'] = $typeOfExercise->name;
            $result[$typeOfExercise->id]['alias'] = $typeOfExercise->alias;
        }
        return $result;
    }

}
