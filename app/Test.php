<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Test extends Model
{
    use \Stevebauman\EloquentTable\TableTrait;
    protected $table = 'tests';

    public static function add($data)
    {
        $test = new self();
        $test->name = $data->name;
        $test->short_name = $data->short_name;
        $test->alias = $data->alias;
        $test->author_id = Auth::user()->id;
        return $test->save();
    }

    public static function edit($id, $data)
    {
        $test = self::find($id);
        $test->name = $data->name;
        $test->short_name = $data->short_name;
        $test->alias = $data->alias;
        $test->author_id = Auth::user()->id;
        return $test->save();
    }

    public static function getRecords()
    {
        $tests = self::all();
        $result = [];
        foreach ($tests as $test) {
            $result[$test->id] = $test->name;
        }
        return $result;
    }

}
