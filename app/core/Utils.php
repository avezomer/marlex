<?php
/**
 * Created by PhpStorm.
 * User: AveZomer
 * Date: 03.06.2018
 * Time: 17:26
 */

namespace App\Core;


use Illuminate\Support\Facades\Auth;

class Utils
{
    /**
     * Cклонение существительных в зависимости от кол-ва<br/>
     * Пример (1 страна, 2 страны, 5 стран) <br/>
     * Вызов <strong>declOfNouns( N, array("страна", "страны", "стран"))</strong>
     * @param int $number количество
     * @param array $titles массив с возможными значениями.
     * @return string код HTML
     */
    public static function declOfNouns($number, array $titles)//
    {
        $cases = [2, 0, 1, 1, 1, 2];
        return $titles[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
    }

    public static function getTimeLineItems()
    {
        $data = \DB::select("
        #новая глава
        SELECT
            chapters.name AS item_name,
            chapters.id AS item_id,
            'MarLex' AS author_name,
            DATE_FORMAT(chapters.created_at, '%d.%m.%Y') AS created_date,
            DATE_FORMAT(chapters.created_at, '%H:%i') AS created_time,
            'chapter' AS type
        FROM
            chapters
        
        UNION ALL
        
        #новая тема
        SELECT
            topics.name AS item_name,
            topics.id AS item_id,
            users.name AS author_name,
            DATE_FORMAT(topics.created_at, '%d.%m.%Y') AS created_date,
            DATE_FORMAT(topics.created_at, '%H:%i') AS created_time,
            'topic' AS type
        FROM
            topics
            JOIN users ON topics.author_id = users.id
        
        UNION ALL
        #новый пост
        SELECT
            chatter_discussion.title AS item_name,
            chatter_discussion.id AS item_id,
            users.name AS author_name,
            DATE_FORMAT(chatter_post.updated_at, '%d.%m.%Y') AS created_date,
            DATE_FORMAT(chatter_post.updated_at, '%H:%i') AS created_time,
            'chatter' AS type
        FROM
            chatter_discussion
            JOIN chatter_post ON chatter_discussion.id = chatter_post.chatter_discussion_id
            JOIN users ON chatter_post.user_id = users.id
            
        ORDER BY created_date DESC, created_time DESC
        ");
        $temp = [];
        foreach ($data as $item) {
            $temp[$item->created_date][] = [
                'item_name' => $item->item_name,
                'item_id' => $item->item_id,
                'author_name' => $item->author_name,
                'created_time' => $item->created_time,
                'type' => $item->type,
            ];
        }

        return $temp;
    }
}