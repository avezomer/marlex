<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    use \Stevebauman\EloquentTable\TableTrait;
    protected $table = 'chapters';

    public static function add($data)
    {
        $chapter = new Chapter();
        $chapter->name = $data->name;
        $chapter->short_name = $data->short_name;
        $chapter->alias = $data->alias;
        return $chapter->save();
    }

    public static function edit($id, $data)
    {
        $chapter = Chapter::find($id);
        $chapter->name = $data->name;
        $chapter->short_name = $data->short_name;
        $chapter->alias = $data->alias;
        return $chapter->save();
    }

    public static function getRecords()
    {
        $chapters = Chapter::all();
        $result = [];
        foreach ($chapters as $chapter) {
            $result[$chapter->id] = $chapter->name;
        }
        return $result;
    }

    public function topics()
    {
        return $this->hasMany('App\Topic', 'chapter_id');
    }

    public static function getChaptersWithTopics()
    {
        $data = \DB::select("
        SELECT
            chapters.id AS chapter_id,
            chapters.name AS chapter_name,
            chapters.short_name AS chapter_short_name,
            chapters.alias AS chapter_alias,
            topics.id AS topic_id,
            topics.name AS topic_name,
            topics.short_name AS topic_short_name,
            topics.alias AS topic_alias
        FROM
            chapters
            JOIN topics on chapters.id = topics.chapter_id");

        $temp = [];
        foreach ($data as $item) {
            $temp[$item->chapter_id]['chapter_id'] = $item->chapter_id;
            $temp[$item->chapter_id]['chapter_name'] = $item->chapter_name;
            $temp[$item->chapter_id]['chapter_short_name'] = $item->chapter_short_name;
            $temp[$item->chapter_id]['chapter_alias'] = $item->chapter_alias;
            $temp[$item->chapter_id]['topics'][$item->topic_id]['topic_id'] = $item->topic_id;
            $temp[$item->chapter_id]['topics'][$item->topic_id]['topic_name'] = $item->topic_name;
            $temp[$item->chapter_id]['topics'][$item->topic_id]['topic_short_name'] = $item->topic_short_name;
            $temp[$item->chapter_id]['topics'][$item->topic_id]['topic_alias'] = $item->topic_alias;
        }
        return $temp;
    }


}
