<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Topic extends Model
{
    use \Stevebauman\EloquentTable\TableTrait;
    protected $table = 'topics';

    public static function add($data)
    {
        $topic = new Topic();
        $topic->name = $data->name;
        $topic->short_name = $data->short_name;
        $topic->alias = $data->alias;
        $topic->text = $data->text;
        $topic->chapter_id = $data->chapter_id;
        $topic->author_id = Auth::user()->id;
        return $topic->save();
    }

    public static function edit($id, $data)
    {
        $topic = Topic::find($id);
        $topic->name = $data->name;
        $topic->short_name = $data->short_name;
        $topic->alias = $data->alias;
        $topic->text = $data->text;
        $topic->chapter_id = $data->chapter_id;
        $topic->author_id = Auth::user()->id;
        return $topic->save();
    }

    public function chapters()
    {
        return $this->belongsTo('App\Chapter', 'chapter_id');
    }

    public function authors()
    {
        return $this->belongsTo('App\User', 'author_id');
    }
}
