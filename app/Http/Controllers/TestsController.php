<?php

namespace App\Http\Controllers;
use App\Exercise;
use App\Test;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;


class TestsController extends Controller
{
    public $rules = [
        'name'       => 'required|unique:tests|max:255',
        'short_name' => 'required|unique:tests|max:50',
        'alias'      => 'required|unique:tests|max:255',
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tests = Test::get();
        return view('tests.index', compact('tests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tests.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        if (Test::add($request)) {
            $request->session()->flash('success', 'Тест успешно добавлен');
        }


        return Redirect::to('tests');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function show(Test $test)
    {
        $testsExercises = Exercise::getModuleExercises($test->id, 'test');
        return view('tests.show', compact('test', 'testsExercises'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id - int
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $test = Test::find($id);
        return view('tests.edit', compact('test'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id - int
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->rules);

        if (Test::edit($id, $request)) {
            $request->session()->flash('success', 'Тест успешно изменён');
            return Redirect::to('tests');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id - int
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $test = Test::find($id);
        $test->delete();

        return Redirect::to('tests');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function evaluate(Request $request)
    {
        Exercise::evaluate($request);
        $request->session()->flash('success', 'Задания выполнены успешно');

        return Redirect::to("tests/{$request->module_id}");

    }

}
