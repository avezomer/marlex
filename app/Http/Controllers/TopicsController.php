<?php

namespace App\Http\Controllers;

use App\Chapter;
use App\Exercise;
use App\Topic;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Redirect;

class TopicsController extends Controller
{
    public $rules = [
        'name'       => 'required|max:255',
        'short_name' => 'required|max:50',
        'alias'      => 'required|max:255',
        'text'       => 'required|min:30',
        'chapter_id' => 'required',
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $topics = Topic::with('authors')->with('chapters')->get();
        return view('topics.index', compact('topics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $chapters = Chapter::getRecords();
        return view('topics.add', compact('chapters'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        if (Topic::add($request)) {
            $request->session()->flash('success', 'Тема успешно добавлена');
        }

        return Redirect::to('topics');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function show(Topic $topic)
    {
        $topicsExercises = Exercise::getModuleExercises($topic->id);
        return view('topics.show', compact('topic', 'topicsExercises'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function edit(Topic $topic)
    {
        $chapters = Chapter::getRecords();
        return view('topics.edit', compact('topic', 'chapters'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id - int
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, $this->rules);

        if (Topic::edit($id, $request)) {
            $request->session()->flash('success', 'Задание успешно изменено');
            return Redirect::to('topics');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id - int
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        // delete
        $topic = Topic::find($id);
        $topic->delete();

        return Redirect::to('topics');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function evaluate(Request $request)
    {
        Exercise::evaluate($request);
        $request->session()->flash('success', 'Задания выполнены успешно');

        return Redirect::to("topics/{$request->module_id}");

    }

}
