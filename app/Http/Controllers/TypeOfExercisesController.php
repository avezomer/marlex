<?php

namespace App\Http\Controllers;
use Auth;
use App\TypeOfExercise;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;


class TypeOfExercisesController extends Controller
{
    public $rules = [
        'name'       => 'required|unique:type_of_exercises|max:255',
        'alias'      => 'required|unique:type_of_exercises|max:255',
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $typeOfExercises = TypeOfExercise::get();
        return view('type_of_exercises.index', compact('typeOfExercises'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('type_of_exercises.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        if (TypeOfExercise::add($request)) {
            $request->session()->flash('success', 'Тип задания успешно добавлен');
        }


        return Redirect::to('type_of_exercises');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TypeOfExercise  $typeOfExercise
     * @return \Illuminate\Http\Response
     */
    public function show(TypeOfExercise $typeOfExercise)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id - int
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $typeOfExercise = TypeOfExercise::find($id);
        return view('type_of_exercises.edit', compact('typeOfExercise'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id - int
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->rules);

        if (TypeOfExercise::edit($id, $request)) {
            $request->session()->flash('success', 'Тип задания успешно изменен');
            return Redirect::to('type_of_exercises');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id - int
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $typeOfExercise = TypeOfExercise::find($id);
        $typeOfExercise->delete();

        return Redirect::to('type_of_exercises');
    }
}
