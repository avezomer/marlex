<?php

namespace App\Http\Controllers;

use App\Exercise;
use App\Test;
use App\Topic;
use Illuminate\Http\Request;
use Auth;
use App\TypeOfExercise;
use Illuminate\Support\Facades\Redirect;

class ExercisesController extends Controller
{
    public $rules = [
        'name'       => 'required|max:255',
        'answer'     => 'required|max:255',
        'text'       => 'required|min:3',
        'points'     => 'required',
        'type_of_exercise' => 'required',
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exercises = Exercise::with('typeOfExercises')->get();
        return view('exercises.index', compact('exercises'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $typeOfExercises = TypeOfExercise::getRecords();
        return view('exercises.add', compact('typeOfExercises'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function link()
    {
        $topics = Topic::get(['id', 'name', 'alias']);
        $tests = Test::get(['id', 'name', 'alias']);
        $exercises = Exercise::getExercisesByTypeOfExercises();
        return view('exercises.link', compact('topics', 'tests', 'exercises'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$this->validate($request, $this->rules);
        if ($request->action === 'link') {
            if (Exercise::link($request)) {
                $request->session()->flash('success', 'Успешная операция');
            }
        } else {
            if (Exercise::add($request)) {
                $request->session()->flash('success', 'Задание успешно добавлено');
            }
        }


        return Redirect::to('exercises');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Exercise  $exercise
     * @return \Illuminate\Http\Response
     */
    public function show(Exercise $exercise)
    {
/*        if (preg_match_all('|<span class="equation">(.+)</span>|isU', $exercise->text, $arr)) {
            $temp = [];
            foreach ($arr[1] as $value) {
                $temp[] = substr($value, 2, -2);
            }
            shuffle($temp);
        }*/
        $exercise = Exercise::with('typeOfExercises')->get()->find($exercise);
        return view('exercises.show', compact('exercise'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id - int
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $exercise = Exercise::find($id);
        $typeOfExercises = TypeOfExercise::getRecords();
        return view('exercises.edit', compact('exercise', 'typeOfExercises'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id - int
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->rules);

        if (Exercise::edit($id, $request)) {
            $request->session()->flash('success', 'Задание успешно изменено');
            return Redirect::to('exercises');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id - int
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $exercise = Exercise::find($id);
        $exercise->delete();

        return Redirect::to('exercises');
    }

    public function deleteResults()
    {
        $userID = \Illuminate\Support\Facades\Auth::user()->id;
        \DB::table('points')->where('user_id', $userID)->delete();
        return Redirect::to('exercises');
    }
}
