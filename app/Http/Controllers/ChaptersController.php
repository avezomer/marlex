<?php

namespace App\Http\Controllers;
use Auth;
use App\Chapter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;


class ChaptersController extends Controller
{
    public $rules = [
        'name'       => 'required|unique:chapters|max:255',
        'short_name' => 'required|unique:chapters|max:50',
        'alias'      => 'required|unique:chapters|max:255',
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $chapters = Chapter::get();
        return view('chapters.index', compact('chapters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('chapters.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        if (Chapter::add($request)) {
            $request->session()->flash('success', 'Глава успешно добавлена');
        }


        return Redirect::to('chapters');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function show(Chapter $chapter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id - int
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $chapter = Chapter::find($id);
        return view('chapters.edit', compact('chapter'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param $id - int
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, $this->rules);

        if (Chapter::edit($id, $request)) {
            $request->session()->flash('success', 'Глава успешно изменена');
            return Redirect::to('chapters');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id - int
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        $chapter = Chapter::find($id);
        $chapter->delete();

        return Redirect::to('chapters');
    }
}
