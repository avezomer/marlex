<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class RolesController extends Controller
{

    public $rules = [
        'name'       => 'required|unique:roles|max:255',
        'guard_name' => 'required|unique:roles|max:255',
    ];

    public function index()
    {
        $roles = Role::get();
        return view('roles.index', compact('roles'));
    }

    public function create()
    {
        return view('roles.add');
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        if (Role::add($request)) {
            $request->session()->flash('success', 'Роль успешно добавлена');
        }


        return Redirect::to('roles');
    }

    public function edit($id)
    {
        $role = Role::find($id);
        return view('roles.edit', compact('role'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, $this->rules);

        if (Role::edit($id, $request)) {
            $request->session()->flash('success', 'Роль успешно изменена');
            return Redirect::to('roles');
        }
    }

    public function delete()
    {}

    public function show($id)
    {}

    public function destroy($id)
    {
        // delete
        $role = Role::find($id);
        $role->delete();

        return Redirect::to('roles');
     }

}
