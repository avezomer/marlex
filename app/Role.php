<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use \Stevebauman\EloquentTable\TableTrait;
    protected $table = 'roles';

    public static function add($data)
    {
        $role = new Role();
        $role->name = $data->name;
        $role->guard_name = $data->guard_name;
        return $role->save();
    }

    public static function edit($id, $data)
    {
        $role = Role::find($id);
        $role->name = $data->name;
        $role->guard_name = $data->guard_name;
        return $role->save();
    }
}
