<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('exercises/link', 'ExercisesController@link');
Route::get('exercises/delete_results', 'ExercisesController@deleteResults');
Route::post('topics/evaluate', 'TopicsController@evaluate');
Route::post('tests/evaluate', 'TestsController@evaluate');

Route::resources([
    'home' => 'HomeController',
    'roles' => 'RolesController',
    'chapters' => 'ChaptersController',
    'type_of_exercises' => 'TypeOfExercisesController',
    'topics' => 'TopicsController',
    'exercises' => 'ExercisesController',
    'tests' => 'TestsController',
]);


//DocumentViewer Library
Route::any('ViewerJS/{all?}', function(){
    return View::make('ViewerJS.index');
});
